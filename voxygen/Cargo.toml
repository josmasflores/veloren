[package]
authors = ["Joshua Barretto <joshua.s.barretto@gmail.com>", "Imbris <imbrisf@gmail.com>"]
default-run = "veloren-voxygen"
edition = "2018"
name = "veloren-voxygen"
version = "0.9.0"
# Cargo thinks it should build the voxygen binary even when a specific bench is specified for building
# Uncomment below and comment out default-run if you want to avoid this
# autobins = false

[package.metadata.nix]
build = true
app = true
desktopFile = "./assets/voxygen/net.veloren.veloren.desktop"
longDescription = """
Veloren is a multiplayer voxel RPG written in Rust.
It is inspired by games such as Cube World, Legend of Zelda: Breath of the Wild, Dwarf Fortress and Minecraft.

This package includes the official client, Voxygen.
"""
runtimeLibs = ["libGL", "xorg.libX11", "xorg.libXcursor", "xorg.libXrandr", "xorg.libXi"]
buildInputs = ["xorg.libxcb"]

[features]
gl = ["gfx_device_gl", "gfx_gl"]
hot-anim = ["anim/use-dyn-lib"]
singleplayer = ["server"]
simd = ["vek/platform_intrinsics"]
tracy = ["common/tracy", "common-ecs/tracy", "common-frontend/tracy", "common-net/tracy", "common-systems/tracy", "common-state/tracy", "client/tracy"]
plugins = ["client/plugins"]

default = ["gl", "singleplayer", "native-dialog", "plugins", "simd"]

[dependencies]
client = {package = "veloren-client", path = "../client"}
common = {package = "veloren-common", path = "../common"}
common-base = {package = "veloren-common-base", path = "../common/base"}
common-ecs = {package = "veloren-common-ecs", path = "../common/ecs"}
common-frontend = {package = "veloren-common-frontend", path = "../common/frontend"}
common-net = {package = "veloren-common-net", path = "../common/net"}
common-systems = {package = "veloren-common-systems", path = "../common/systems"}
common-state = {package = "veloren-common-state", path = "../common/state"}

anim = {package = "veloren-voxygen-anim", path = "anim"}
i18n = {package = "veloren-i18n", path = "i18n"}

# Graphics
gfx = "0.18.2"
gfx_device_gl = {version = "0.16.2", optional = true}
gfx_gl = {version = "0.6.1", optional = true}
glutin = "0.26.0"
old_school_gfx_glutin_ext = "0.26"
winit = {version = "0.24.0", features = ["serde"]}

# Ui
conrod_core = {git = "https://gitlab.com/veloren/conrod.git", branch="copypasta_0.7"}
conrod_winit = {git = "https://gitlab.com/veloren/conrod.git", branch="copypasta_0.7"}
euc = "0.5.0"
iced = {package = "iced_native", git = "https://github.com/hecrj/iced", rev = "8d882d787e6b7fd7c2435f42f82933e2ed904edf"}
iced_winit = {git = "https://github.com/hecrj/iced", rev = "8d882d787e6b7fd7c2435f42f82933e2ed904edf"}
window_clipboard = "0.2"
glyph_brush = "0.7.0"
keyboard-keynames = { git = "https://gitlab.com/Frinksy/keyboard-keynames.git", rev = "a97ae509cdb9dc70cf1bf0af762d2d1d3a0d6e0c" }

# ECS
specs = {git = "https://github.com/amethyst/specs.git", rev = "5a9b71035007be0e3574f35184acac1cd4530496"}
specs-idvs = { git = "https://gitlab.com/veloren/specs-idvs.git", rev = "b65fb220e94f5d3c9bc30074a076149763795556" }

# Mathematics
vek = {version = "=0.14.1", features = ["serde"]}

# Controller
gilrs = {version = "0.8.0", features = ["serde-serialize"]}

# Singleplayer
server = {package = "veloren-server", path = "../server", optional = true}

# Utility
backtrace = "0.3.40"
bincode = "1.3.1"
chrono = { version = "0.4.9", features = ["serde"] }
cpal = "0.13"
copy_dir = "0.1.2"
crossbeam = "0.8.0"
# TODO: remove
directories-next = "2.0"
dot_vox = "4.0"
enum-iterator = "0.6"
strum = "0.20"
strum_macros = "0.20"
glsl-include = "0.3.1"
guillotiere = "0.6"
hashbrown = {version = "0.9", features = ["rayon", "serde", "nightly"]}
image = {version = "0.23.12", default-features = false, features = ["ico", "png"]}
lazy_static = "1.4.0"
native-dialog = { version = "0.5.2", optional = true }
num = "0.4"
ordered-float = { version = "2.0.1", default-features = false }
rand = "0.8"
rodio = {version = "0.13", default-features = false, features = ["vorbis"]}
ron = {version = "0.6", default-features = false}
serde = {version = "1.0", features = [ "rc", "derive" ]}
treeculler = "0.2"
tokio = { version = "1", default-features = false, features = ["rt-multi-thread"] }
num_cpus = "1.0"
# vec_map = { version = "0.8.2" }
# inline_tweak = "1.0.2"
itertools = "0.10.0"

# Tracy
tracing = "0.1"

[target.'cfg(target_os = "macos")'.dependencies]
dispatch = "0.1.4"
# This needs to be backdated for the OSX build, potentially because the SDK is not OSX 11.0 compatible yet.
# This line should be removed when possible because coreaudio-sys is not a direct dep of voxygen.
coreaudio-sys = { version = "=0.2.5", default-features = false }

[target.'cfg(windows)'.build-dependencies]
winres = "0.1"

[dev-dependencies]
criterion = "0.3"
world = {package = "veloren-world", path = "../world"}
rayon = "1.5.0"

[[bench]]
harness = false
name = "meshing_benchmark"
